import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading!: boolean;

  display: boolean = false;
  error!: string;

  myForm: FormGroup = this.fb.group({
    email: ['', [Validators.required]],
    password: ['', [Validators.required]]
  });
  
  // private authService: AuthService,
  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  login(): void {
    console.log('se undio');
    const { email, password } = this.myForm.value;
    this.authService.login(email,password)
    .catch(err => {
      this.error = err.message;
      this.display = true;
      console.log('por que no entraste' + err);
    });; 
    // this.authService.login( email, password)
    //                 .catch(err => {
    //                   this.error = err.message;
    //                   this.display = true;
    //                   this.store.dispatch( new DeactivateLoadingAction() );
    //                 });
  }

}
