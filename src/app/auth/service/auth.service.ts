import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public afAuth: AngularFireAuth,
    private router: Router
    ) { }

    login(email: string, password: string): Promise<void>{
      return this.afAuth
        .signInWithEmailAndPassword(email, password)
        .then( () => {
          this.router.navigate(['/home']);
        });
    }

  register(email: string, password: string, username: string): Promise<void> {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then( () => {
        console.log('Se creo el usuario en firebase');
        this.router.navigate(['/auth/login'])
      });
  }

  logout(): void{
    this.router.navigate(['/auth/login']);
    this.afAuth.signOut();
  }

  isAuth(): Observable<boolean> {
    return this.afAuth.authState
                            .pipe(
                              map( fbUser =>  {
                                if ( fbUser == null){
                                  this.router.navigate(['/auth/login']);
                                }
                                return fbUser != null;
                              })
                            );
  }
}
