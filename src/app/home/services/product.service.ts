import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';


@Injectable()
export class ProductService {

    status: string[] = ['OUTOFSTOCK', 'INSTOCK', 'LOWSTOCK'];

    productNames: string[] = [
        "La Semilla Feliz", 
        "Los Hombres del Norte", 
        "Viaje Sentimental", 
        "Viaje al centro de la tierra", 
        "Batman: The Killing Joke", 
        "Arsène Lupin", 
        "Álgebra BALDOR",
        "Aproximación a la Ing. Software",
        "Gravity Falls",
        "Gaming Set",
        "WIGETTA y el Báculo Dorado",
        "Mi Viaje sin Ti",
        "Domina tu mente",
        "Yo Antes de Ti",
        "Fundamentos en Sistemas Operativos",
        "Invincible",
        "Kick Ass",
        "Las Ventajas de Ser Invisible",
        "Nacho Lee",
        "CUADRADO: Panita esta es mi historia",
        "LOS AMANTES DE ARGEL",
        "Despúes",
        "Asados",
        "COMIDA RAW. Recetas y Preparación        ",
        "Comida Sana en BOW",
        "Una Tierra Prometida ",
        "civil War II",
        "El último día",
        "El Cosmos y la Materia Oscura",
        "Las Almas de Brandon",
    ];

    constructor(private http: HttpClient) { }

    getProductsSmall() {
        return this.http.get<any>('assets/products-small.json')
        .toPromise()
        .then(res => <Product[]>res.data)
        .then(data => { return data; });
    }

    getProducts() {
        return this.http.get<any>('assets/products.json')
        .toPromise()
        .then(res => <Product[]>res.data)
        .then(data => { return data; });
    }

    getProductsWithOrdersSmall() {
        return this.http.get<any>('assets/products-orders-small.json')
        .toPromise()
        .then(res => <Product[]>res.data)
        .then(data => { return data; });
    }

    generatePrduct(): Product {
        const product: Product =  {
            id: this.generateId(),
            name: this.generateName(),
            description: "Product Description",
            price: this.generatePrice(),
            quantity: this.generateQuantity(),
            category: "Product Category",
            inventoryStatus: this.generateStatus(),
            rating: this.generateRating()
        };

        product.image = product.name!.toLocaleLowerCase().split(/[ ,]+/).join('-')+".jpg";;
        return product;
    }

    generateId() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        
        return text;
    }

    generateName() {
        return this.productNames[Math.floor(Math.random() * Math.floor(30))];
    }

    generatePrice() {
        return Math.floor(Math.random() * Math.floor(299)+1);
    }

    generateQuantity() {
        return Math.floor(Math.random() * Math.floor(75)+1);
    }

    generateStatus() {
        return this.status[Math.floor(Math.random() * Math.floor(3))];
    }

    generateRating() {
        return Math.floor(Math.random() * Math.floor(5)+1);
    }
}