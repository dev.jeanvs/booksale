// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBj6iRrAQ-Rxf_iekj47uN8o8pyXUu0e1o",
    authDomain: "booksale-f0e83.firebaseapp.com",
    projectId: "booksale-f0e83",
    storageBucket: "booksale-f0e83.appspot.com",
    messagingSenderId: "616948513704",
    appId: "1:616948513704:web:7ac4c2cc1b7dc931593b9d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
